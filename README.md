# dnsresolv

A tiny tool to resolve domains and ip addresses.

## Installation

From crates:
```shell
cargo install dnsresolv
```

## Usage

Resolve many domains:
```shell
$ cat domains.txt | dnsresolv
domain.test 1.1.1.1
domain2.test 1.1.1.2
```
