mod args;
mod readin;

use args::Arguments;
use futures::{stream, StreamExt};
use stderrlog;

use std::net::IpAddr;

use trust_dns_resolver::config::LookupIpStrategy;
use trust_dns_resolver::{system_conf, AsyncResolver};

use log::{info, warn};

fn init_log(verbosity: usize) {
    stderrlog::new()
        .module(module_path!())
        .verbosity(verbosity)
        .init()
        .expect("Error initiating log");
}

#[tokio::main]
async fn main() {
    let args = Arguments::parse_args();
    init_log(args.verbosity);

    let (resolver_config, mut resolver_opts) = system_conf::read_system_conf()
        .expect("Error parsing system configuration");

    resolver_opts.ip_strategy = LookupIpStrategy::Ipv4Only;
    resolver_opts.attempts = 1;
    resolver_opts.num_concurrent_reqs = args.workers;
    resolver_opts.timeout = args.timeout;

    let resolver = AsyncResolver::tokio(resolver_config, resolver_opts)
        .await
        .expect("failed to connect resolver");

    let client = &resolver;

    let fetches = stream::iter(readin::read_inputs(args.targets))
        .map(|host| async move {
            info!("Resolving {}", host);
            let domains: Vec<String>;
            let ips: Vec<String>;

            match host.parse::<IpAddr>() {
                Ok(ip) => match client.reverse_lookup(ip).await {
                    Ok(response) => {
                        domains =
                            response.iter().map(|d| d.to_string()).collect();
                        ips = vec![host];
                    }
                    Err(err) => {
                        warn!("Error in query {}", err);
                        return;
                    }
                },
                Err(_) => match client.ipv4_lookup(host.clone()).await {
                    Ok(response) => {
                        domains = vec![host];
                        ips =
                            response.iter().map(|ip| ip.to_string()).collect();
                    }
                    Err(err) => {
                        warn!("Error in query {}", err);
                        return;
                    }
                },
            }

            if domains.len() > 0 && ips.len() > 0 {
                println!("{} {}", domains.join(","), ips.join(","))
            }
        })
        .buffer_unordered(args.workers)
        .collect::<Vec<()>>();

    fetches.await;
}
