use clap::{App, Arg, ArgMatches};
use std::time::Duration;

fn args() -> App<'static, 'static> {
    App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(
            Arg::with_name("host")
                .takes_value(true)
                .help("Specify a host (IP or domain) or a file with hosts")
                .multiple(true)
                .use_delimiter(true),
        )
        .arg(
            Arg::with_name("workers")
                .short("w")
                .long("workers")
                .takes_value(true)
                .help("Number of workers")
                .default_value("10"),
        )
        .arg(
            Arg::with_name("timeout")
                .long("timeout")
                .short("T")
                .takes_value(true)
                .help("Timeout (milliseconds)")
                .default_value("5000"),
        )
        .arg(
            Arg::with_name("verbosity")
                .short("v")
                .multiple(true)
                .help("Verbosity"),
        )
}

pub struct Arguments {
    pub targets: Vec<String>,
    pub workers: usize,
    pub timeout: Duration,
    pub verbosity: usize,
}

impl Arguments {
    pub fn parse_args() -> Self {
        let matches = args().get_matches();
        let workers = matches
            .value_of("workers")
            .unwrap()
            .parse()
            .expect("Invalid threads value");
        let timeout = Duration::from_millis(
            matches
                .value_of("timeout")
                .unwrap()
                .parse()
                .expect("Invalid timeout value"),
        );

        let verbosity = matches.occurrences_of("verbosity") as usize;

        return Self {
            targets: Self::parse_targets(&matches),
            workers,
            timeout,
            verbosity,
        };
    }

    fn parse_targets(matches: &ArgMatches) -> Vec<String> {
        if let Some(hosts) = matches.values_of("host") {
            return hosts.map(|s| s.to_string()).collect();
        }
        return Vec::new();
    }
}
